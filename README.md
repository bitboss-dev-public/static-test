# Static test

## Test’s goal

Evaluate your ability to reproduce a static web page (HTML, CSS) using Tailwind CSS, starting from a mockup intentionally not explicative in all its parts.

It will be evaluated also the ability to define the behavior of the web page not explicitly described (e.g. the positioning of the planets in the various resolutions).

## Guide

1. Clone the repository;
2. Edit the index.html file to get to the target. Inside it you will find the script tag of tailwind's Play CDN. ([https://tailwindcss.com/docs/installation/play-cdn](https://tailwindcss.com/docs/installation/play-cdn));
3. Eventually add a text file in which you can indicate the time taken to perform the test and any feedback;
4. Create a merge request when you think you are done;

## Requirements

- Use Tailwind CSS to make the web page as similar as possible to the mockup
- Modify tailwind.config and insert custom css always inside index.html. Do not use external style sheets
- Limit as much as possible the writing of custom css.

## Useful links

At this link you will find the mockup of the web page.

[https://invis.io/HC12MLV9YKSN#/466320004_Test](https://invis.io/HC12MLV9YKSN#/466320004_Test)
